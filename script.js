const container=document.querySelector('#boxes-container');

function createGrid(){
	const gridSize=document.getElementById('qty').value;

	if(gridSize<1||gridSize>100){
		return alert('Number is either a 0 and even bellow,or is too big.');
	}
	if(container.innerHTML!=""){
		container.innerHTML="";
	}
	//accessing the --gridSize property from the css file
	container.style.setProperty('--gridSize',gridSize);
	container.style.setProperty('--gridGap',gridSize<41?"1px":0);

	for (var i = 0; i < gridSize**2; i++) {
		createBox();
	}
}
function createBox(){
	var idNum=1;
	const newDiv=document.createElement('div');
	newDiv.setAttribute('id',idNum);
	newDiv.setAttribute('class',"boxDiv");
	container.appendChild(newDiv);
	++idNum;

	newDiv.addEventListener('mouseover',function(e){
		e.target.style.background='#D8E782';
	});
}